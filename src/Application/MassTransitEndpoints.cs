﻿using IWG.Heimdall.Application.Weather.Consumers.SetWeatherForecast;
using IWG.Heimdall.Contracts.Weather.API.SetWeatherForecast;
using MassTransit;

namespace IWG.Heimdall.Application;

public class MassTransitEndpoints
{
    public static void Register(MassTransit.Azure.ServiceBus.Core.IServiceBusBusFactoryConfigurator cfg)
    {
        cfg.SubscriptionEndpoint<SetWeatherForecastRequest>("weather-forecast", e =>
        {
            e.Consumer<SetWeatherForecastConsumer>();
        });
    }
}
