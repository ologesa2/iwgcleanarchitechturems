﻿namespace IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;

public record GetWeatherForecastResponse(List<WeatherForecast> Temperatures);
