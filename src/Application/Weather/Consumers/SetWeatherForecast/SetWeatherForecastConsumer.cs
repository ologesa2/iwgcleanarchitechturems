﻿using IWG.Heimdall.Contracts.Weather.API.SetWeatherForecast;
using MassTransit;

namespace IWG.Heimdall.Application.Weather.Consumers.SetWeatherForecast;

internal class SetWeatherForecastConsumer : IConsumer<SetWeatherForecastRequest>
{
    public async Task Consume(ConsumeContext<SetWeatherForecastRequest> context)
    {
        Console.WriteLine($"Okay {context.Message} received");
    }
}
