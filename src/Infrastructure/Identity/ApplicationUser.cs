﻿using Microsoft.AspNetCore.Identity;

namespace IWG.Heimdall.Infrastructure.Identity;

public class ApplicationUser : IdentityUser
{
}
