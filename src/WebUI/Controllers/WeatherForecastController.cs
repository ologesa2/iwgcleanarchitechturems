﻿using AutoMapper;
using IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;
using Microsoft.AspNetCore.Mvc;

namespace IWG.Heimdall.WebUI.Controllers;

public class WeatherForecastController : ApiControllerBase
{
    private readonly IMapper _mapper;

    public WeatherForecastController(IMapper mapper)
    {
        _mapper = mapper;
    }



    [HttpGet]
    public async Task<IEnumerable<Contracts.WeatherForecast>> Get()
    {

        var client = Mediator.CreateRequestClient<GetWeatherForecastRequest>();
        var getWeatherForecastRequest = new GetWeatherForecastRequest(new DateTime(2020, 10, 10), new DateTime(2020, 10, 11));
        var getWeatherForecastResponse = await client.GetResponse<GetWeatherForecastResponse>(getWeatherForecastRequest);

        return getWeatherForecastResponse.Message.Temperatures.Select(_mapper.Map<Contracts.WeatherForecast>);
    }
}
